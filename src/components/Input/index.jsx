import styles from "./index.module.scss";

export function Input(props) {
  return <input className={styles.input} {...props} />;
}
