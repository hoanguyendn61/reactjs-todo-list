import { Task } from "../Task";

import styles from "./index.module.scss";

export function Tasks({
  tasks,
  searchTaskName,
  onRemoveTask,
  onUpdateTask,
  onChangeCompletedTask,
}) {
  const isVisibleTask = (task) => {
    const taskName = task.title.toLocaleLowerCase();
    return taskName.includes(searchTaskName);
  };
  return (
    <ul className={styles.tasks}>
      {tasks.map(
        (task) =>
          isVisibleTask(task) && (
            <Task
              task={task}
              key={task.id}
              onUpdate={onUpdateTask}
              onRemove={onRemoveTask}
              onChangeCompleted={onChangeCompletedTask}
            />
          )
      )}
    </ul>
  );
}
