import styles from "./index.module.scss";
import { useState } from "react";
import { Input } from "./../Input";
export function UpdateDialog({ onDialog, title }) {
  const [taskName, setTaskName] = useState(title);
  return (
    <div className={styles.modal} onClick={() => onDialog(false)}>
      <div
        className={styles.modal__content}
        onClick={(e) => e.stopPropagation()}
      >
        <Input
          type="text"
          defaultValue={title}
          placeholder="Name of task"
          onChange={(event) => {
            setTaskName(event.target.value);
          }}
        />
        <div className={styles.modal__content__button}>
          <button
            onClick={() => onDialog(false)}
            className={styles.modal__content__button_cancel}
          >
            Cancel
          </button>
          <button
            onClick={() => onDialog(true, taskName)}
            className={styles.modal__content__button_confirm}
          >
            Update
          </button>
        </div>
      </div>
    </div>
  );
}
