import { FaTrashAlt, FaPencilAlt } from "react-icons/fa";

import styles from "./index.module.scss";

export function Task({ task, onRemove, onUpdate, onChangeCompleted }) {
  return (
    <li className={`${styles.task} ${task.completed ? styles.completed : ""}`}>
      <input
        type="checkbox"
        checked={task.completed}
        className={styles.task__checkbox}
        onChange={(e) => onChangeCompleted(task.id)}
      />

      <span className={styles.task__name}>{task.title}</span>
      <button
        type="button"
        className={styles.task__button}
        onClick={() => onUpdate(task)}
      >
        <FaPencilAlt size={16} />
      </button>
      <button
        type="button"
        className={styles.task__button}
        onClick={() => onRemove(task)}
      >
        <FaTrashAlt size={16} />
      </button>
    </li>
  );
}
