import styles from "./index.module.scss";
export function ConfirmDialog({ onDialog, title }) {
  return (
    <div className={styles.modal} onClick={() => onDialog(false)}>
      <div
        className={styles.modal__content}
        onClick={(e) => e.stopPropagation()}
      >
        <h4>Are you sure you want to delete</h4>
        <p
          style={{
            marginTop: "8px",
          }}
        >
          {title}
        </p>
        <div className={styles.modal__content__button}>
          <button
            onClick={() => onDialog(false)}
            className={styles.modal__content__button_cancel}
          >
            Cancel
          </button>
          <button
            onClick={() => onDialog(true)}
            className={styles.modal__content__button_confirm}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}
