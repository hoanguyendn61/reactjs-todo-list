import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { taskApi } from "../../api/taskApi";

// First, create the thunk to fetch all tasks.
export const fetchAllTasks = createAsyncThunk(
  "tasks/fetchAll",
  async (payload) => {
    const response = await taskApi.getAll();
    return response;
  }
);

const initialState = {
  todos: [],
  status: "idle",
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
      taskApi.add(action.payload);
    },
    changeCompleteTodo: (state, action) => {
      const task = state.todos.find((todo) => todo.id === action.payload.id);
      task.completed = !task.completed;
      taskApi.update(task);
    },
    updateTodo: (state, action) => {
      const task = state.todos.find((todo) => todo.id === action.payload.id);
      task.title = action.payload.title;
      taskApi.update(task);
    },
    removeTodo: (state, action) => {
      state.todos = state.todos.filter((todo) => todo.id !== action.payload.id);

      taskApi.delete(action.payload.id);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAllTasks.fulfilled, (state, action) => {
      state.todos = action.payload;
    });
  },
});

export const { addTodo, removeTodo, changeCompleteTodo, updateTodo } =
  todoSlice.actions;

export const selectTodo = (state) => state.todo;

export default todoSlice.reducer;
