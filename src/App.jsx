import { Input } from "./components/Input";
import { Form } from "./components/Form";
import { Tasks } from "./components/Tasks";
import { ConfirmDialog } from "./components/ConfirmDialog";
import { UpdateDialog } from "./components/UpdateDialog";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles/app.module.scss";
import {
  selectTodo,
  fetchAllTasks,
  addTodo,
  changeCompleteTodo,
  removeTodo,
  updateTodo,
} from "./features/todo/todoSlice";
import { useEffect, useMemo, useState } from "react";
function App() {
  const dispatch = useDispatch();
  const { todos } = useSelector(selectTodo);
  const [searchTaskName, setSearchTaskName] = useState("");
  const [deleteDialog, setDeleteDialog] = useState({
    isOpen: false,
    task: null,
  });
  const [updateDialog, setUpdateDialog] = useState({
    isOpen: false,
    task: null,
  });

  const onChangeCompleted = (id) => {
    dispatch(changeCompleteTodo({ id }));
  };
  const handleDeleteDialog = (isOpen, task) => {
    setDeleteDialog({
      isOpen,
      task,
    });
  };
  const handleUpdateDialog = (isOpen, task) => {
    setUpdateDialog({
      isOpen,
      task,
    });
  };
  const onRemoveTask = (task) => {
    handleDeleteDialog(true, task);
  };
  const onUpdateTask = (task) => {
    handleUpdateDialog(true, task);
  };
  const areUSureDelete = (isConfirm) => {
    if (isConfirm) {
      dispatch(removeTodo({ id: deleteDialog.task.id }));
    }
    handleDeleteDialog(false, "");
  };
  const areUSureUpdate = (isConfirm, newTaskTitle) => {
    if (isConfirm) {
      dispatch(
        updateTodo({
          id: updateDialog.task.id,
          title: newTaskTitle,
        })
      );
    }
    handleUpdateDialog(false, "");
  };
  const onHandleSubmit = (newTask) => {
    dispatch(addTodo({ ...newTask }));
  };

  const onChangeSearchTaskName = (e) => {
    const taskSearch = e.target.value.toLocaleLowerCase();
    setSearchTaskName(taskSearch);
  };
  // useEffect
  useEffect(() => {
    dispatch(fetchAllTasks());
  }, []);

  const totalTask = useMemo(() => {
    return todos.length;
  }, [todos]);

  const totalCompletedTasks = useMemo(() => {
    return todos.filter((todo) => todo.completed).length;
  });
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        {deleteDialog.isOpen && (
          <ConfirmDialog
            onDialog={areUSureDelete}
            title={deleteDialog.task.title}
          />
        )}
        {updateDialog.isOpen && (
          <UpdateDialog
            onDialog={areUSureUpdate}
            title={updateDialog.task.title}
          />
        )}
        <h1>TODOLIST</h1>
        <Form onSubmit={onHandleSubmit} />
        <hr />
        <Input
          type="text"
          value={searchTaskName}
          placeholder="Search tasks..."
          onChange={onChangeSearchTaskName}
        />

        <Tasks
          tasks={todos}
          searchTaskName={searchTaskName}
          onChangeCompletedTask={onChangeCompleted}
          onUpdateTask={onUpdateTask}
          onRemoveTask={onRemoveTask}
        />

        <footer className={styles.footer}>
          <h6>
            Total tasks:
            <span>{totalTask}</span>
          </h6>
          <h6>
            Total completed tasks:
            <span>{totalCompletedTasks}</span>
          </h6>
        </footer>
      </div>
    </div>
  );
}

export default App;
