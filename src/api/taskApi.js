import axiosClient from "./axiosClient";
export const taskApi = {
  getAll: async () => {
    const url = "/tasks";
    const response = await axiosClient.get(url);
    return response;
  },
  getById: async (id) => {
    const url = `/tasks/${id}`;
    const response = await axiosClient.get(url);
    return response;
  },
  add: (task) => {
    const url = "/tasks";
    return axiosClient.post(url, task);
  },
  update: (task) => {
    const url = `/tasks/${task.id}`;
    return axiosClient.patch(url, task);
  },
  delete: (id) => {
    const url = `/tasks/${id}`;
    return axiosClient.delete(url);
  },
};
